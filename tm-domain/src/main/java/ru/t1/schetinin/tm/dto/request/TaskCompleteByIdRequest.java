package ru.t1.schetinin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskCompleteByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}