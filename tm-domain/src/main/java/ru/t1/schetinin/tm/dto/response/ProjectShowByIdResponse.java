package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@NotNull final Project project) {
        super(project);
    }
}
