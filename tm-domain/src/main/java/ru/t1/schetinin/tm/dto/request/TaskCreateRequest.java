package ru.t1.schetinin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskCreateRequest extends AbstractUserRequest {

    private String name;

    private String description;

    public TaskCreateRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

}