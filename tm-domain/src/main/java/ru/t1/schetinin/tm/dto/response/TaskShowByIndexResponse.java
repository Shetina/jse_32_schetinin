package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@NotNull final Task task) {
        super(task);
    }

}