package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractProjectResponse {

    @NotNull
    private List<Project> projects;

    public ProjectListResponse(@NotNull final List<Project> projects) {
        this.projects = projects;
    }

}