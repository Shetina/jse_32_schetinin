package ru.t1.schetinin.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.dto.request.AbstractUserRequest;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.user.AccessDeniedException;
import ru.t1.schetinin.tm.model.User;

public abstract class AbstractEndpoint {

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role){
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request){
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final  IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
