package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.UserLoginRequest;
import ru.t1.schetinin.tm.dto.request.UserLogoutRequest;
import ru.t1.schetinin.tm.dto.request.UserViewProfileRequest;
import ru.t1.schetinin.tm.dto.response.UserLoginResponse;
import ru.t1.schetinin.tm.dto.response.UserLogoutResponse;
import ru.t1.schetinin.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpointClient extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse profile(@NotNull UserViewProfileRequest request);

}