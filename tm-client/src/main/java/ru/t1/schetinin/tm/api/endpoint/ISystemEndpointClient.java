package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.ServerAboutRequest;
import ru.t1.schetinin.tm.dto.request.ServerVersionRequest;
import ru.t1.schetinin.tm.dto.response.ServerAboutResponse;
import ru.t1.schetinin.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpointClient extends IEndpointClient {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}