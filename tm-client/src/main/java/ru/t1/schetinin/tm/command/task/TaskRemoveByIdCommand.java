package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.schetinin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setId(id);
        getTaskEndpoint().taskRemoveById(request);
    }

}