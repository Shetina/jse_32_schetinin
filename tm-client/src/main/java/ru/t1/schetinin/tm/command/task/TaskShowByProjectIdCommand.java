package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show tasks list by project id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest();
        request.setProjectId(projectId);
        @Nullable final List<Task> tasks = getTaskEndpoint().taskShowByProjectId(request).getTasks();
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

}