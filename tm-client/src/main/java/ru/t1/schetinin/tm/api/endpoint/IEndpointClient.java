package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IEndpointClient {

    void setSocket(@Nullable Socket socket);

    Socket connect();

    void disconnect();

    void setHost(@NotNull String host);

    void setPort(@NotNull Integer port);

}