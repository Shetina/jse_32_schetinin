package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.TaskStartByIdRequest;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {
    //not use in server
    @NotNull
    private static final String NAME = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(id);
        getTaskEndpoint().taskStartById(request);
    }

}