package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}