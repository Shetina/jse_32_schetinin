package ru.t1.schetinin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.ProjectStartByIndexRequest;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {
    //not use in server
    @NotNull
    private static final String NAME = "project-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().projectStartByIndex(request);
    }

}