package ru.t1.schetinin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.schetinin.tm.dto.request.UserLoginRequest;
import ru.t1.schetinin.tm.dto.request.UserLogoutRequest;
import ru.t1.schetinin.tm.dto.request.UserViewProfileRequest;
import ru.t1.schetinin.tm.dto.response.UserLoginResponse;
import ru.t1.schetinin.tm.dto.response.UserLogoutResponse;
import ru.t1.schetinin.tm.dto.response.UserViewProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse profile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

}