package ru.t1.schetinin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {
    //not use in server
    @NotNull
    private static final String NAME = "project-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setId(id);
        getProjectEndpoint().projectStartById(request);
    }

}