package ru.t1.schetinin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.endpoint.IProjectEndpointClient;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.dto.response.*;

@NoArgsConstructor
public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser().getEmail());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);

        System.out.println(projectClient.createProject(new ProjectCreateRequest("HELLO2", "WORLD2")));
        System.out.println(projectClient.projectList(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}