package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.dto.response.*;

public interface ITaskEndpointClient extends IEndpointClient {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request);

}