package ru.t1.schetinin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.schetinin.tm.api.service.ICommandService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    protected ISystemEndpointClient getSystemEndpoint() {
        return getServiceLocator().getSystemEndpointClient();
    }

}