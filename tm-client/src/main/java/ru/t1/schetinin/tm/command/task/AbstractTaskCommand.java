package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.schetinin.tm.api.service.IProjectTaskService;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpointClient getTaskEndpoint() {
        return getServiceLocator().getTaskEndpointClient();
    }


    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}