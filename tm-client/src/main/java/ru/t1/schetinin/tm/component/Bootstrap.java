package ru.t1.schetinin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.schetinin.tm.api.endpoint.*;
import ru.t1.schetinin.tm.api.repository.ICommandRepository;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.client.*;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.command.server.ConnectCommand;
import ru.t1.schetinin.tm.command.server.DisconnectCommand;
import ru.t1.schetinin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.schetinin.tm.exception.system.CommandNotSupportedException;
import ru.t1.schetinin.tm.repository.CommandRepository;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.SystemUtil;
import ru.t1.schetinin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.schetinin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @Getter
    @NotNull
    private final ISystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NotNull
    private final IDomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NotNull
    private final IProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final ITaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @Getter
    @NotNull
    private final IAuthEndpointClient authEndpointClient = new AuthEndpointClient();

    @Getter
    @NotNull
    private final IUserEndpointClient userEndpointClient = new UserEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void connect() {
        processCommand(ConnectCommand.NAME);
    }

    private void disconnect() {
        processCommand(DisconnectCommand.NAME);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void initCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        //if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***");
        fileScanner.stop();
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        prepareStartup();
        initCommands();
    }

}