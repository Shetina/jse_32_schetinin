package ru.t1.schetinin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.schetinin.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {

    }

    @NotNull
    public IDomainEndpointClient getDomainEndpoint() {
        return getServiceLocator().getDomainEndpointClient();
    }

}